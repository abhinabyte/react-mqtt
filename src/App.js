import React, { useState, Fragment } from 'react';
import './App.css';

var mqtt = require('mqtt');

var options = {
  protocol: 'mqtts',
  // protocol: 'mqtt',
  clientId: 'b0908853',
  username: 'mqttuser',
  password: 'mqttpasswd',
  clean: false,
  keepalive: 1,
  reconnectPeriod: 3000
};

var client  = mqtt.connect('wss://dev.wrkspot.com:8083/mqtt', options);
// var client  = mqtt.connect('ws://127.0.0.1:8083/mqtt', options);

client.subscribe('/wrkspot/dashboard');
let result = 1

function App() {
  // var note;
  client.on('message', (topic, message) => {
    // note = message.toString();
    result++
    setMesg(result)
    console.log(result)
    });

  const [mesg, setMesg] = useState(<Fragment><em>nothing heard</em></Fragment>);

  return (
    <div className="App">
    <header className="App-header">
    <h1>wrkspot dashboard</h1>
    <p>Incoming event: {mesg}</p>
		<p>
		<a href="https://wrkspot.com"    
		style={{
			color: 'white'
		}}>wrkspot</a>
		</p>
		</header>
		</div>
  );
}

export default App;